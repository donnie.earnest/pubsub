FROM centos:7.6.1810

COPY . .

ENV PYTHONIOENCODING=utf8

RUN yum install -y epel-release; \
    yum install -y python36 python36-devel; \
    yum clean all; rm -rf /etc/cache/yum

RUN python3 -m pip install -r /usr/src/requirements.txt
